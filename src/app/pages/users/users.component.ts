import { Component } from "@angular/core";
import { UserList } from "src/app/interfaces/user.interface";
import { DataService } from "src/app/services/data.service";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent {
  users!: UserList[];
  constructor(private dataService: DataService) {
    this.dataService.getUser().subscribe((response) => {
      console.log(response);
      this.users = response;
    });
  }
}
