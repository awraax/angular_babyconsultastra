import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent {
  formRegister: FormGroup;

  error = "";

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      noTelp: ['', [Validators.required, Validators.minLength(8)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      is_admin: ['', [Validators.required]]
    });
  }

  get confirmPassword() {
    return (
      this.formRegister.value.password ==
      this.formRegister.value.confirmPassword
    );
  }

  get errorControl() {
    return this.formRegister.controls;
  }

  doRegister() {
    const payload = {
      username: this.formRegister.value.username,
      email: this.formRegister.value.email,
      noTelp: this.formRegister.value.noTelp,
      password: this.formRegister.value.password,
      is_admin: this.formRegister.value.is_admin,
    };
    this.authService.register(payload).subscribe((response) => {
      console.log(response);
    });
  }
}
