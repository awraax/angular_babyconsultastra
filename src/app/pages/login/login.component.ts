import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  formLogin: FormGroup;

  error = "";

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  get errorControl() {
    return this.formLogin.controls;
  }

  doLogin() {
    console.log(this.formLogin);
    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };
    this.authService.login(payload).subscribe(
      (response) => {
        console.log(response);

        if (response.data.jwtrole === "admin") {
          localStorage.setItem("token", response.data.token);
          this.router.navigate(["/admin/dashboard"]);
          Swal.fire({
            title: "Successfully Login!",
            text:
              "You have successfully login  as " +
              response.data.jwtrole +
              "! 🥳",
            icon: "success",
            confirmButtonColor: "#0046e6",
            confirmButtonText: "OK",
          });
          localStorage.setItem("token", response.data.token);
        }
        if (response.data.jwtrole === "user") {
          Swal.fire({
            title: "Access Denied!",
            text: "Only Admin can access this page.",
            icon: "error",
            confirmButtonColor: "#0046e6",
            confirmButtonText: "OK",
          });
        }
      },
      (error) => {
        console.log(error);
        Swal.fire({
          title: "Uh Oh!",
          text: "You have entered wrong password or username!",
          icon: "error",
          confirmButtonColor: "#0046e6",
          confirmButtonText: "OK",
        });
        this.error = error.error.message;
      }
    );
  }
}
