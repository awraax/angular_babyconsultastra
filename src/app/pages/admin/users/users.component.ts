import { Component, TemplateRef } from "@angular/core";
import { UserList } from "src/app/interfaces/user.interface";
import { UsersService } from "src/app/services/users.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { map, Subject, switchMap } from "rxjs";
import { AuthService } from "src/app/services/auth.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import Swal from "sweetalert2";
// import { Pipe, PipeTransform } from '@angular/core';
// import 'lodash-es/lodash';

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent {
  users!: UserList[];
  modalRef?: BsModalRef;

  // image!: string;
  // imageFile!: File;
  formAdd: FormGroup;
  refresh = new Subject<void>();

  userDetail: UserList = {
    username: "",
    email: "",
    noTelp: "",
    password: "",
    is_admin: false,
    // image: "",

  };

  loadData() {
    this.userServices.listUsers().subscribe((response) => {
      console.log(response);
      this.users = response;
    });
  }

  constructor(
    private userServices: UsersService,
    private modalService: BsModalService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      username: ["", [Validators.required, Validators.minLength(4)]],
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      noTelp: ["", [Validators.required]],
      is_admin: ["admin"],
      // image: ["", [Validators.required]],
    });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.doCheckDetail;
  }

  // showPreview(event: any) {
  //   if (event) {
  //     const file = event.target.files[0];
  //     this.imageFile = file;
  //     const reader = new FileReader();
  //     reader.onload = () => {
  //       this.image = reader.result as string;
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // }

  doAddUser() {
    const payload = {
      username: this.formAdd.value.username,
      email: this.formAdd.value.email,
      noTelp: this.formAdd.value.noTelp,
      password: this.formAdd.value.password,
      is_admin: this.formAdd.value.is_admin,
      // image: val.data,
    };
    console.log(payload)
    this.authService.register(payload).subscribe(
      response => {
        console.log(response)
        Swal.fire({
          title: "Successfully Add The Data!",
          text: "You have successfully add 1 user data 🥳",
          icon: "success",
          confirmButtonColor: "#0046e6",
          confirmButtonText: "OK",
        });
        this.loadData();
      },
      error => {
        console.log(error)
      }
    )
  }

  doCheckDetail(data: UserList) {
    console.log(data);
    // console.log(data.image);
    this.userDetail = data;
  }
}
