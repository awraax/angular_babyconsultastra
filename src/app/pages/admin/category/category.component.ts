import { Component, TemplateRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { map, Subject, switchMap, takeUntil } from "rxjs";
import { CategoryList } from "src/app/interfaces/category.interface";
import { CategoryService } from "src/app/services/category.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
})
export class CategoryComponent {
  category!: CategoryList[];
  formAdd: FormGroup;
  formEdit: FormGroup;
  modalRef?: BsModalRef;


  
  refresh = new Subject<void>();

  categoryDetail: CategoryList = {
    kategori_id: 0,
    nama_kategori: "",
  };

  constructor(
    private categoryService: CategoryService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      nama_kategori: ["", [Validators.required]]
    });
    this.formEdit = this.formBuilder.group({
      nama_kategori: [""]
    });
  }

  loadData() {
    this.categoryService
      .listCategory()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.category = response;
      });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  // showPreview(event: any) {
  //   if (event) {
  //     const file = event.target.files[0];
  //     this.imageFile = file;
  //     const reader = new FileReader();
  //     reader.onload = () => {
  //       this.image = reader.result as string;
  //     };
  //     reader.readAsDataURL(file);
  //   }
  // }

  doAddCategory() {
      Swal.fire({
        title: "Success!",
        text: "You have successfully add 1 category data.",
        icon: "success",
        confirmButtonColor: "#0046e6",
        confirmButtonText: "OK",
      });
      this.categoryService
        .listCategory()
        .pipe(
          switchMap((val) => {
            const payload = {
              nama_kategori: this.formAdd.value.nama_kategori,
            };
            return this.categoryService.addCategory(payload).pipe(map((val)=> val))
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }


  doEditCategory() {
      Swal.fire({
        title: "Success!",
        text: "You have successfully edit category data.",
        icon: "success",
        confirmButtonColor: "#0046e6",
        confirmButtonText: "OK",
      });
      const payload = {
        nama_kategori: this.formEdit.value.nama_kategori,
      };
      this.categoryService
        .editCategory(payload, this.categoryDetail.kategori_id)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  

  doDeleteCategory() {
      Swal.fire({
        title: "Success!",
        text: "You have successfully edit category data.",
        icon: "success",
        confirmButtonColor: "#0046e6",
        confirmButtonText: "OK",
      });
      const payload = {
        nama_kategori: this.formEdit.value.nama_kategori
      };
      this.categoryService
        .deleteCategory(payload, this.categoryDetail.kategori_id)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }


  doEditModal(data: CategoryList) {
    console.log(data);
    this.categoryDetail = data;
    this.formEdit.controls["nama_kategori"].patchValue(data.nama_kategori);
  }

  doDeleteModal(data: CategoryList) {
    console.log(data);
    this.categoryDetail = data;
  }
}
