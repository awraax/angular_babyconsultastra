import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AdminComponent } from "./admin.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutsModule } from "src/app/layouts/layouts.module";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { SettingsComponent } from "./settings/settings.component";
import { UsersComponent } from "./users/users.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CategoryComponent } from "./category/category.component";
import { TransactionComponent } from "./transaction/transaction.component";
import { DoctorComponent } from './doctor/doctor.component';

const routes: Routes = [
  {
    path: "",
    component: AdminComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
      },
      {
        path: "settings",
        component: SettingsComponent,
      },
      {
        path: "category",
        component: CategoryComponent,
      },
      {
        path: "users",
        component: UsersComponent,
      },
      {
        path: "doctor",
        component: DoctorComponent,
      },
      {
        path: "transaction",
        component: TransactionComponent,
      },
    ],
  },
];
@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    SettingsComponent,
    CategoryComponent,
    UsersComponent,
    DoctorComponent,
    TransactionComponent,
    DoctorComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AdminModule {}
