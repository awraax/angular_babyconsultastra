import { Component, TemplateRef } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { map, Subject, switchMap, takeUntil } from "rxjs";
import { TransactionList } from "src/app/interfaces/transaction.interface";
import { TransactionService } from "src/app/services/transaction.service";

@Component({
  selector: "app-transaction",
  templateUrl: "./transaction.component.html",
  styleUrls: ["./transaction.component.scss"],
})
export class TransactionComponent {
  modalRef?: BsModalRef;

  transactions!: TransactionList[];

  refresh = new Subject<void>();

  transactionDetail!: TransactionList;

  constructor(private transactionService: TransactionService) {
    this.loadData();
  }

  loadData() {
    this.transactionService
      .listTransaction()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log("listTransaction", response);
        this.transactions = response;
      });
  }
}
