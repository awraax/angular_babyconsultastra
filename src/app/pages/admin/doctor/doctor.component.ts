import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { DoctorList } from 'src/app/interfaces/doctor.interface';
import { CategoryService } from 'src/app/services/category.service';
import { DoctorService } from 'src/app/services/doctor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss']
})
export class DoctorComponent {
  formAddDoctor: FormGroup;
  formEditDoctor: FormGroup;
  modalRef?: BsModalRef;

  doctor!: DoctorList[];

  refresh = new Subject<void>();

  categoryList$ = this.categoryService.listCategory();

  doctorDetail!: DoctorList;

  constructor(
    private doctorService: DoctorService,
    private categoryService: CategoryService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAddDoctor = this.formBuilder.group({
      namaDokter: ["", [Validators.required]],
      jenisKelamin: ["", [Validators.required]],
      asalRumahSakit: ["", [Validators.required]],
      jadwal: ["", [Validators.required]],
      doctorCategory: ["", [Validators.required]],
    });

    this.formEditDoctor = this.formBuilder.group({
      namaDokter: [""],
      jenisKelamin: [""],
      asalRumahSakit: [""],
      jadwal: [""],
      doctorCategory: [""]
    });
  }

  loadData() {
    this.doctorService
      .listDoctor()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log("listdoctor", response);
        this.doctor = response;
      });
  }

  get errorControl() {
    return this.formAddDoctor.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  doAddDoctor() {
    Swal.fire({
      title: "Success!",
      text: "You have successfully add 1 doctor data.",
      icon: "success",
      confirmButtonColor: "#0046e6",
      confirmButtonText: "OK",
    });
    this.doctorService
      .listDoctor()
      .pipe(
        switchMap((val) => {
          const payload = {
            namaDokter: this.formAddDoctor.value.namaDokter,
            jenisKelamin: this.formAddDoctor.value.jenisKelamin,
            asalRumahSakit: this.formAddDoctor.value.asalRumahSakit,
            jadwal: this.formAddDoctor.value.jadwal,
            doctorCategory: {
              kategori_id: this.formAddDoctor.value.doctorCategory
            }
          };
          return this.doctorService
            .addDoctor(payload)
            .pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
  }

  doEditDoctor() {
    Swal.fire({
      title: "Success!",
      text: "You have successfully edit product data.",
      icon: "success",
      confirmButtonColor: "#0046e6",
      confirmButtonText: "OK",
    });
    const payload = {
      namaDokter: this.formEditDoctor.value.namaDokter,
      jenisKelamin: this.formEditDoctor.value.jenisKelamin,
      asalRumahSakit: this.formEditDoctor.value.asalRumahSakit,
      jadwal: this.formEditDoctor.value.jadwal,
      doctorCategory: {
        kategori_id: this.formEditDoctor.value.doctorCategory
      }
    };
    this.doctorService
      .editDoctor(payload, this.doctorDetail.dokter_id)
      .pipe(map((val) => val))

      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
  }

  doDeleteModal(data: DoctorList){
    this.doctorDetail = data
  }

  doDeleteDoctor() {
    console.log("Delete")
    // const payload = {
    //   namaDokter: this.formAddDoctor.value.namaDokter,
    //   jenisKelamin: this.formAddDoctor.value.jenisKelamin,
    //   asalRumahSakit: this.formAddDoctor.value.asalRumahSakit,
    //   jadwal: this.formAddDoctor.value.jadwal,
    //   doctorCategory: {
    //     kategori_id: this.formAddDoctor.value.doctorCategory
    //   }
    // };
    this.doctorService
      .deleteDoctor(this.doctorDetail.dokter_id)
      .pipe(map((val) => val))

      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
    Swal.fire({
      title: "Success!",
      text: "You have successfully delete product data.",
      icon: "success",
      confirmButtonColor: "#0046e6",
      confirmButtonText: "OK",
    });
  }

  doEditModal(data: DoctorList) {
    console.log(data);
    this.doctorDetail = data;
    this.formEditDoctor.controls["namaDokter"].patchValue(data.namaDokter);
    this.formEditDoctor.controls["jenisKelamin"].patchValue(data.jenisKelamin);
    this.formEditDoctor.controls["asalRumahSakit"].patchValue(data.asalRumahSakit);
    this.formEditDoctor.controls["jadwal"].patchValue(data.jadwal);
    this.formEditDoctor.controls["kategori"].patchValue(data.doctorCategory);
  }
}
