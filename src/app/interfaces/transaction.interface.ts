export interface TransactionList {
  transaksi_id: number;
  tanggalTransaksi: string;
  totalHarga: number;
  // status: boolean;
  // user: User;
  doctor: Doctor;
}

// export interface User {
//   user_id: number,
//   username: string,
//   email: string,
//   noTelp: string,
//   password: string,
//   is_admin: boolean;
//   admin?: boolean;
//   // image?: string;
// }

export interface Doctor {
  dokter_id: number;
  namaDokter: string;
  jenisKelamin: string;
  asalRumahSakit: string;
  jadwal: string;
  doctorCategory: CategoryList;
}

export interface CategoryList {
  kategori_id: number;
  nama_kategori: string;
}

// export interface Category {
//   id_category: number;
//   categoryName: string;
//   categoryImage: string;
// }

// export interface Brand {
//   id_brand: number;
//   brandName: string;
//   brandImage: string;
// }

// export interface DietType {
//   id_diet_type: number;
//   dietTypeName: string;
//   dietTypeDesc: string;
//   dietTypeImage: string;
// }
