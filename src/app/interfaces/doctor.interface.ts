export interface DoctorList {
    dokter_id: number;
    namaDokter: string;
    jenisKelamin: string;
    asalRumahSakit: string;
    jadwal: string;
    doctorCategory: CategoryList;
}
export interface CategoryList {
    kategori_id: number;
    nama_kategori: string;
}

