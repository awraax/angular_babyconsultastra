export interface UserList {
  username: string,
  email: string,
  noTelp: string,
  password: string,
  is_admin: boolean;
  admin?: boolean;
  // image?: string;
}

export interface ResponseUploadImage {
  image: string;
}
