import { LOCALE_ID, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { UsersComponent } from "./pages/users/users.component";
import { AdminComponent } from "./pages/admin/admin.component";
import { ModalModule } from "ngx-bootstrap/modal";
import { HttpInterceptorCore } from "./core/http.interceptor";
import localId from "@angular/common/locales/id";
import { registerLocaleData } from "@angular/common";

registerLocaleData(localId, "id");

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UsersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ModalModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorCore, multi: true },
    {
      provide: LOCALE_ID,
      useValue: "id",
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
