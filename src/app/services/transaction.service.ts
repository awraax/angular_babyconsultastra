import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { ResponseBase } from "../interfaces/response.interface";
import { TransactionList } from "../interfaces/transaction.interface";

@Injectable({
  providedIn: "root",
})
export class TransactionService {
  private baseApi = "http://localhost:8080/baby-consult";
  constructor(private httpClient: HttpClient) {}

  listTransaction(): Observable<TransactionList[]> {
    return this.httpClient
      .get<ResponseBase<TransactionList[]>>(
        `${this.baseApi}/transaction-history`
      )
      .pipe(
        map((val) => {
          return val.data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }
}
