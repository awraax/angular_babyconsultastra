import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { ResponseBase } from "../interfaces/response.interface";
import { UserList } from "../interfaces/user.interface";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  private baseApi = "http://localhost:8080/baby-consult";
  constructor(private httpClient: HttpClient) {}

  listUsers(): Observable<UserList[]> {
    return this.httpClient
      .get<ResponseBase<UserList[]>>(`${this.baseApi}/user`)
      .pipe(
        map((val) => {
          const data: UserList[] = [];
          for (let item of val.data) {
            data.push({
              username: item.username,
              email: item.email,
              noTelp: item.noTelp,
              password: item.password,
              is_admin: item.is_admin,
              // image: `${this.baseApi}/files/${item.image}:.+`,
            });
          }
          return data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  // uploadImage(data: File): Observable<ResponseBase<string>> {
  //   const file = new FormData();
  //   file.append("file", data, data.name);
  //   return this.httpClient.post<ResponseBase<string>>(
  //     `${this.baseApi}/uploads`,
  //     file
  //   );
  // }
}
