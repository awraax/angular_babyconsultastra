import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { DoctorList } from '../interfaces/doctor.interface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private baseApi = "http://localhost:8080/baby-consult";
  constructor(private httpClient: HttpClient) { }

  listDoctor(): Observable<DoctorList[]> {
    return this.httpClient
      .get<ResponseBase<DoctorList[]>>(`${this.baseApi}/data-doctor`)
      .pipe(
        map((val) => {
          return val.data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  addDoctor(payload: {
    namaDokter: string;
    jenisKelamin: string;
    asalRumahSakit: string;
    jadwal: string;
    doctorCategory: {
      kategori_id: number;
    }
  }) {
    // const data = {
    //   doctor: [payload],
    // };

    return this.httpClient.post(
      this.baseApi + `/doctor/${payload.doctorCategory.kategori_id}`,
      payload
    );
  }

  editDoctor(
    payload: {
      namaDokter: string;
      jenisKelamin: string;
      asalRumahSakit: string;
      jadwal: string;
      doctorCategory: {
        kategori_id: number;
      }
    },
    id: number
  ) {
    return this.httpClient.put(
      this.baseApi + "/update-doctor/" + id,
      payload
    );
  }

  deleteDoctor(
    // payload: {
    //   namaDokter: string;
    //   jenisKelamin: string;
    //   asalRumahSakit: string;
    //   jadwal: string;
    //   doctorCategory: {
    //     kategori_id: number;
    //   }
    // },
    id: number
  ) {
    return this.httpClient.delete(this.baseApi + "/delete-doctor/" + id);
  }
}
