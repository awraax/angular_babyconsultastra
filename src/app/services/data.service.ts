import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserList } from "../interfaces/user.interface";

@Injectable({
  providedIn: "root",
})
export class DataService {
  public baseHarga = 20000;
  private baseApi = "https://jsonplaceholder.typicode.com";
  constructor(private httpClient: HttpClient) {}

  getUser(): Observable<UserList[]> {
    return this.httpClient.get<UserList[]>(this.baseApi + "/users");
  }

  private baseApiFP = "http://localhost:8080";
}
