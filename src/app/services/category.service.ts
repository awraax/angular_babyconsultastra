import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, map, Observable } from "rxjs";
import { CategoryList } from "../interfaces/category.interface";
import { ResponseBase } from "../interfaces/response.interface";

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  private baseApi = "http://localhost:8080/baby-consult";
  constructor(private httpClient: HttpClient) {}

  listCategory(): Observable<CategoryList[]> {
    return this.httpClient
      .get<ResponseBase<CategoryList[]>>(`${this.baseApi}/get-category-doctor`)
      .pipe(
        map((val) => {
          const data: CategoryList[] = [];
          for (let item of val.data) {
            data.push({
              kategori_id: item.kategori_id,
              nama_kategori: item.nama_kategori,
            });
          }
          return data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  addCategory(payload: { nama_kategori: string}) {
    return this.httpClient.post(this.baseApi + "/category-doctor", payload);
  }

  editCategory(
    payload: { nama_kategori: string },
    id: number
  ) {
    return this.httpClient.put(
      this.baseApi + "/update-category-doctor/" + id,
      payload
    );
  }

  deleteCategory(
    payload: { nama_kategori: string },
    id: number
  ) {
    return this.httpClient.delete(this.baseApi + "/delete-category-doctor/" + id);
  }
}
